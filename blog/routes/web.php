<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});

Route::get('/test/{angka}', function ($angka){
    return view('test',["angka" => $angka]);
});

Route::get('/halo/{nama}', function ($nama){
    return "halo $nama";
});

Route::get('/form','RegisterController@form');

Route::get('/sapa','RegisterController@sapa');
Route::get('/sapa','RegisterController@sapa_post');

Route::get('/master', function () {
    return view('adminlte.master');
});
*/
//latihan laravel tutorial
/*
Route::get('/items', function () {
    return view('items.index');
});
*/
//tugas laravel 2
/*
Route::get('/data-table', function () {
    return view('data-table.data-tables');
});

Route::get('/', function () {
    return view('data-table.table');
});
*/
//Laravel CRUD dengan Query Builder
Route::get('/pertanyaan', 'PertanyaanController@index');

Route::get('/pertanyaan/create', 'PertanyaanController@create');

Route::post('/pertanyaan', 'PertanyaanController@store');
Route::get('/pertanyaan/{id}', 'PertanyaanController@show');
Route::get('/pertanyaan/{id}/edit', 'PertanyaanController@edit');
Route::put('/pertanyaan/{id}', 'PertanyaanController@update');
Route::delete('/pertanyaan/{id}', 'PertanyaanController@destroy');
